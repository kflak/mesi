(
fork{
    ~mesiShuffle = MBMesiShuffle.new(db: -5).play(~luulurLeft, ~luulurRight);
    ~mesiShuffle.buf = KF.buf[\mesiFull][0..1];
    ~mesiShuffle.stride = 0.3;
    ~mesiShuffle.overlap = 4;
    
    60.wait;

    ~beeShuffle = MBBeeShuffle.new(db: -26, fadeTime: 90).play(9, 10);
    ~beeShuffle.releaseMul = 10;
    Tdef(\bee, {
        var duration = 120;
        var minRate = Env([1, 0.1], duration).asStream;
        var maxRate = Env([1, 0.3], duration).asStream;
        var minRateModFreq = Env([0.1, 0.8], duration).asStream;
        var maxRateModFreq = Env([0.0, 20.0], duration).asStream;
        var minRateModDepth = Env([0.0, 0.1], duration).asStream;
        var maxRateModDepth = Env([0.0, 0.9], duration).asStream;
        var minPitch = Env([-6, -12], duration).asStream;
        var maxPitch = Env([-6, -12], duration).asStream;
        var minPitchModFreq = Env([0.0, 0.0], duration).asStream;
        var maxPitchModFreq = Env([0.0, 0.2], duration).asStream;
        var minPitchModDepth = Env([0.0, 0.02], duration).asStream;
        var maxPitchModDepth = Env([0.0, 0.02], duration).asStream;
        var overlap = Pseg(Pwhite(0.1, 2), Pwhite(1, 10)).asStream;
        var tFreq = Pseg(Pwhite(1, 20), Pwhite(1, 10)).asStream;

       loop {
            ~beeShuffle.minRate = minRate.next;
            ~beeShuffle.maxRate = maxRate.next;
            ~beeShuffle.minRateModFreq = minRateModFreq.next;
            ~beeShuffle.maxRateModFreq = maxRateModFreq.next;
            ~beeShuffle.minRateModDepth = minRateModDepth.next;
            ~beeShuffle.maxRateModDepth = maxRateModDepth.next;
            ~beeShuffle.minPitch = minPitch.next;
            ~beeShuffle.maxPitch = maxPitch.next;
            ~beeShuffle.minPitchModFreq = minPitchModFreq.next;
            ~beeShuffle.maxPitchModFreq = maxPitchModFreq.next;
            ~beeShuffle.minPitchModDepth = minPitchModDepth.next;
            ~beeShuffle.maxPitchModDepth = maxPitchModDepth.next;
            ~beeShuffle.overlap = overlap.next;
            ~beeShuffle.tFreq = tFreq.next;
            1.wait;
        }
    }).play;
}
)
