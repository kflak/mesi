(
KF.audioDir = "/home/kf/sc/mesi/audio";
KF.start;
)
(
~mesiGrain.free;
~mesiGrain = MBMesilator.new(db: -15, threshold:0.02).play;
// ~mesiGrain.buf = KF.buf[\mesi];
)

~mesiGrain.grainSizeDeviation = 0.03;
~mesiGrain.db = -10;
~mesiGrain.threshold = 0.01;
~mesiGrain.timeStretch = 32;
~mesiGrain.grainSize = 0.005;
~mesiGrain.overlap = 2;
~mesiGrain.grainSizeDeviation = 0.006;
~mesiGrain.release;

NetAddr.langPort;


~mesiShuffle = MBMesiShuffle.new(db: -10).play;
~mesiShuffle.minDuration = 1;
~mesiShuffle.maxDuration = 2;
~mesiShuffle.minRate = 1;
~mesiShuffle.maxRate = 1;
~mesiShuffle.minRateModFreq = 0.0;
~mesiShuffle.maxRateModFreq = 20.0;
~mesiShuffle.minRateModDepth = 0.1;
~mesiShuffle.maxRateModDepth = 0.9;

(
~mesiShuffle.minRate = 0.1;
~mesiShuffle.maxRate = 0.3;
~mesiShuffle.minRateModFreq = 0.0;
~mesiShuffle.maxRateModFreq = 20.0;
~mesiShuffle.minRateModDepth = 0.1;
~mesiShuffle.maxRateModDepth = 0.9;

~mesiShuffle.minPitch = -24.midiratio;
~mesiShuffle.maxPitch = -12.midiratio;
~mesiShuffle.minPitchModFreq = 0.0;
~mesiShuffle.maxPitchModFreq = 0.2;
~mesiShuffle.minPitchModDepth = 0.01;
~mesiShuffle.maxPitchModDepth = 0.02;
)

~mesiShuffle.db = -20;
~mesiShuffle.threshold = 0.02;
~mesiShuffle.release(2);
~mesiShuffle.releaseMul = 3;
~mesiShuffle.legato;


(
Tdef(\mesi, {
    var duration = 60;
    var minRate = Env([0.1, 1], duration).asStream;
    var maxRate = Env([0.3, 1], duration).asStream;
    var minRateModFreq = Env([0.0, 0.0], duration).asStream;
    var maxRateModFreq = Env([20.0, 0.0], duration).asStream;
    var minRateModDepth = Env([0.1, 0.0], duration).asStream;
    var maxRateModDepth = Env([0.9, 0.0], duration).asStream;
    var minPitch = Env([-24, 0], duration).asStream;
    var maxPitch = Env([-12, 0], duration).asStream;
    var minPitchModFreq = Env([0.0, 0.0], duration).asStream;
    var maxPitchModFreq = Env([0.2, 0.0], duration).asStream;
    var minPitchModDepth = Env([0.01, 0.0], duration).asStream;
    var maxPitchModDepth = Env([0.02, 0.0], duration).asStream;

    inf.do{
        ~mesiShuffle.minRate = minRate.next;
        ~mesiShuffle.maxRate = maxRate.next;
        ~mesiShuffle.minRateModFreq = minRateModFreq.next;
        ~mesiShuffle.maxRateModFreq = maxRateModFreq.next;
        ~mesiShuffle.minRateModDepth = minRateModDepth.next;
        ~mesiShuffle.maxRateModDepth = maxRateModDepth.next;
        ~mesiShuffle.minPitch = minPitch.next;
        ~mesiShuffle.maxPitch = maxPitch.next;
        ~mesiShuffle.minPitchModFreq = minPitchModFreq.next;
        ~mesiShuffle.maxPitchModFreq = maxPitchModFreq.next;
        ~mesiShuffle.minPitchModDepth = minPitchModDepth.next;
        ~mesiShuffle.maxPitchModDepth = maxPitchModDepth.next;
        1.wait;
    }
}).play;
)

(
~windy = MBWindy.new().play;
~windy.minAmp = -30;
~windy.db = 12;
)
~windy.free;



(
~klang = MBKlang.new(maxAmp: -10, threshold: 0.05).play;
~klang.octave = #[2, 3, 4, 5];
~klang.degrees = #[0, 2, 4, 5];
~klang.modDepth = 0.2;
~klang.scale = Scale.minor;
~klang.durMin = 1.0;
~klang.durMax = 2;
~klang.attackMul = 4;
~klang.releaseMul = 30;
~klang.root = 1;
)

~klang.free;

fork{
    ~paperTear = MBPapertearGranulated.new(db: -6).play;
    ~paperTear.db = -6;
    ~paperTear.release;

    ~screamsFromHell = MBScreamsFromHell.new(db: -20).play(14);
    ~screamsFromHell.release;

    ~screa1msFromHellLow = MBScreamsFromHellLow.new(db: -20).play(13);
    ~screamsFromHellLow.release;

    ~didg = MBDidgGranulated.new(db: -3).play;
    ~didg.release;
}

~stormThunder = MBStormThunderShuffle.new().play;
~stormThunder.release(30);


(
~mesiShuffle = MBMesiShuffle.new.play(~luulurLeft, ~luulurRight);
~mesiShuffle.buf = KF.buf[\mesiFull][0..1];
~mesiShuffle.stride = 0.3;
Tdef(\mesi, {
    loop {
        ~mesiShuffle.minPitch = KF.mbData[~luulurLeft].x.linlin(0.0, 1.0, -12, 12);
        ~mesiShuffle.maxPitch = KF.mbData[~luulurRight].x.linlin(0.0, 1.0, -12, 12);
        0.1.wait;
    }
}).play;
)

(
~beeShuffle.release;
~beeShuffle = MBBeeShuffle.new.play;
Tdef(\bee, {
    var duration = 120;
    var minRate = Env([0.1, 1], duration).asStream;
    var maxRate = Env([0.3, 1], duration).asStream;
    var minRateModFreq = Env([0.0, 0.0], duration).asStream;
    var maxRateModFreq = Env([20.0, 0.0], duration).asStream;
    var minRateModDepth = Env([0.1, 0.0], duration).asStream;
    var maxRateModDepth = Env([0.9, 0.0], duration).asStream;
    var minPitch = Env([-24, 12], duration).asStream;
    var maxPitch = Env([-12, 12], duration).asStream;
    var minPitchModFreq = Env([0.0, 0.0], duration).asStream;
    var maxPitchModFreq = Env([0.2, 0.1], duration).asStream;
    var minPitchModDepth = Env([0.01, 0.02], duration).asStream;
    var maxPitchModDepth = Env([0.02, 0.05], duration).asStream;

    inf.do{
        ~beeShuffle.minRate = minRate.next;
        ~beeShuffle.maxRate = maxRate.next;
        ~beeShuffle.minRateModFreq = minRateModFreq.next;
        ~beeShuffle.maxRateModFreq = maxRateModFreq.next;
        ~beeShuffle.minRateModDepth = minRateModDepth.next;
        ~beeShuffle.maxRateModDepth = maxRateModDepth.next;
        ~beeShuffle.minPitch = minPitch.next;
        ~beeShuffle.maxPitch = maxPitch.next;
        ~beeShuffle.minPitchModFreq = minPitchModFreq.next;
        ~beeShuffle.maxPitchModFreq = maxPitchModFreq.next;
        ~beeShuffle.minPitchModDepth = minPitchModDepth.next;
        ~beeShuffle.maxPitchModDepth = maxPitchModDepth.next;
        1.wait;
    }
}).play;
)

~gremlins = MBGremlins.new().play;
~gremlins.minPitch = -12;
~gremlins.maxPitch = 12;

~windWhistle = MBWindWhistle.new().play;
